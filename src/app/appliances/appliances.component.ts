import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appliances',
  templateUrl: './appliances.component.html',
  styleUrls: ['./appliances.component.scss']
})
export class AppliancesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  vtype = '';
  vcontact = '';
  vaddress = '';
  vcity = '';
  vprice = '';
  vdetails = '';
  vimage = '';
  selectedIndex = '';
  isEditBtnClicked = "no";
  
  
  userList:any = []
  

  submit(){
    let user ={
      vtype:this.vtype,
      vcontact:this.vcontact,
      vcity:this.vcity,
      vprice:this.vprice,
      vdetails:this.vdetails,
      vimage:this.vimage,
      vaddress:this.vaddress
      }
      this.userList.push(user)
      this.clear()
  }

  clear(){
    this.vtype="";
    this.vcontact="";
    this.vcity="";
    this.vprice="";
    this.vdetails="";
    this.vimage="";
    this.vaddress=""
  }
  delete(idx:any){
    console.log('idx',idx);
    this.userList.splice(idx,1);

  }
  edit(idx:any){
    this.isEditBtnClicked="yes";
    this.selectedIndex = idx;
    this.vtype=this.userList[idx].vtype;
    this.vcontact=this.userList[idx].vcontact;
    this.vcity=this.userList[idx].vcity;
    this.vprice=this.userList[idx].vprice;
    this.vdetails=this.userList[idx].vdetails;
    this.vimage=this.userList[idx].vimage;
    this.vaddress=this.userList[idx].vaddress;
  }
  update(){
    this.userList[this.selectedIndex].vtype = this.vtype;
    this.userList[this.selectedIndex].vcontact = this.vcontact;
    this.userList[this.selectedIndex].vaddress = this.vaddress;
    this.userList[this.selectedIndex].vcity = this.vcity;
    this.userList[this.selectedIndex].vprice = this.vprice;
    this.userList[this.selectedIndex].vdetails = this.vdetails;
    this.userList[this.selectedIndex].vimage = this.vimage;
    this.clear();
    this.isEditBtnClicked="no";
  }
}
